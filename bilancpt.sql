-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 11 Septembre 2020 à 15:00
-- Version du serveur :  5.7.31-0ubuntu0.18.04.1
-- Version de PHP :  7.3.21-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bilancpt`
--

-- --------------------------------------------------------

--
-- Structure de la table `sauvegarde`
--

CREATE TABLE `sauvegarde` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `repas` varchar(255) NOT NULL,
  `chargegly` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `nom`, `pseudo`, `email`, `password`) VALUES
(1, 'rouger', 'oceane', 'rouger.oceane20@gmail.com', '$2y$10$EPuIpXq/SCzVnsga2IY7ZO9EkAK/HxmnFSd1ujHFPwTHjBY4pqHCq'),
(2, 'nico', 'nico', 'nico@gmail.com', '$2y$10$J4bw2c5N9YXIiERAkDFvfeI6fP.tYuGMiPzAISIW5uSUSBeuqu3VG'),
(3, 'hello', 'hello', 'hello@gmail.com', '$2y$10$/iuTeKkNnnAJpSIk5aUn4.X4PgA1.dFhdlQEgDi6a67O0j5eykE/i'),
(4, 'lolo', 'lolo', 'lolo@gmail.com', '$2y$10$tQAF2GPy5SEQFKTdC1s9Te94ynuu.WXwqaMzvZJeHuwaiq9rYR5g2'),
(5, 'roro', 'roro', 'roro@gmail.com', '$2y$10$I1U6ouVArM4m49oFoYVStumJsa5P7xKsLs7v22.EZVpxZBItx553i');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `sauvegarde`
--
ALTER TABLE `sauvegarde`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `sauvegarde`
--
ALTER TABLE `sauvegarde`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
